<?php

class sqlTodoList
{
    const URL = "sqlite:info";
    private $connection;

    function __construct()
    {
        $this->connection = new PDO(self::URL);
        $this->connection -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    function add_todo_item($todoItem)
    {
        if (isset($todoItem)) {
            $statement = $this->connection->prepare(
                "insert into person(firstName, lastName)
                      values (:firstName, :lastName)");
            $statement->bindValue(":firstName", $todoItem->firstname);
            $statement->bindValue(":lastName", $todoItem->lastname);
            $statement->execute();

            $last_id = $this->connection->lastInsertId();

            foreach ($todoItem->phones as $phone){
                $statement = $this->connection->prepare("insert into numbers (number, person_id) VALUES (:phone, :person_id)");
                $statement->bindValue(":phone", $phone);
                $statement->bindValue(":person_id", $last_id);
                $statement->execute();
            }
        }
    }

    function get_todo_items()
    {
        $statement = $this->connection->prepare(
            "select firstname, lastname, number, person.person_id from person, numbers where person.person_id = numbers.person_id");
        $statement->execute();

        $todoItems = [];
        foreach ($statement as $row) {
            $id = $row["person_id"];
            if (isset($todoItems[$id])){
                $todoItem = $todoItems[$id];
                $todoItem->add_phone($row["number"]);
            }
            else {
                $todoItem = new TodoItem($row["firstname"], $row["lastname"], $row["number"], $row["person_id"]);
                $todoItems[$todoItem->person_id] = $todoItem;
            }
        }
        return $todoItems;
    }

    function delete_all_todo_items()
    {
        $statement = $this->connection->prepare(
            "delete from person");
        $statement->execute();

        $statement = $this->connection->prepare(
            "delete from numbers");
        $statement->execute();
    }

    function delete_todo_item($id)
    {

        $statement = $this->connection->prepare(
            "delete from person where person_id = :id");
        $statement->bindValue(":id", $id);
        $statement->execute();

        $statement = $this->connection->prepare(
            "delete from numbers where person_id = :id");
        $statement->bindValue(":id", $id);
        $statement->execute();
    }
}