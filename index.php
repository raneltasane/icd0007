<?php
        require_once("sqlTodoList.php");
        require_once("todoItem.php");
        require_once("lib/tpl.php");

        $cmd = isset($_GET["cmd"]) ? $_GET["cmd"] : "view";

        $personDao = new sqlTodoList();


        if ($cmd == "view") {
            $todoItems = $personDao->get_todo_items();
            $data = ['$todoItems' => $todoItems];
            print render_template("tpl/main.html", $data);
        }

        else if ($cmd == "add") {
            print render_template("tpl/add.html");
        }

        else if ($cmd == "delete") {
            if(isset($_POST["id"])){
                $personDao->delete_todo_item($_POST["id"]);
            }
            $todoItems = $personDao->get_todo_items();
            $data = ['$todoItems' => $todoItems];
            print render_template("tpl/main.html", $data);
        }

        else if ($cmd == "deleteAll") {
            if (isset($_POST["deleteAll"])) {
                $personDao->delete_all_todo_items();
            }

            $data['$messages'] = ["Deleted all messages!"];
            print render_template("tpl/main.html", $data);
        }

        else if ($cmd == "add_item") {
            $data = [];

            $firstName = $_POST["firstName"];
            $lastName = $_POST['lastName'];
            $phone = $_POST["phone1"];
            $phone2 = $_POST["phone2"];
            $phone3 = $_POST["phone3"];

            if(isset($firstName) && $firstName != "" && isset($lastName) && $lastName != "" && isset($phone) && $phone != ""){
                $person = new TodoItem($firstName, $lastName, $phone);
                if (isset($phone2) && $phone2 != ""){
                    $person->add_phone($phone2);
                }
                if(isset($phone3) && $phone3 != ""){
                    $person->add_phone($phone3);
                }
                $personDao->add_todo_item($person);
            }
            header("Location: ?cmd=view");
        }
