<?php
const DATA_FILE = "data.txt";
require_once("index.php");

function add_todo_item_firstname($firstName, $lastName, $phone) {
    if (isset($firstName) && $firstName != "" && isset($lastName) && $lastName != "" && isset($phone) && $phone != "") {
        $todoItems = get_todo_items();
        $item = $firstName . " " . $lastName . " " . $phone;
        if (!in_array($item, $todoItems)) {
            file_put_contents(DATA_FILE, $item . PHP_EOL, FILE_APPEND);
            header("Location: ?cmd=view");
        }
        else{
            $data['$messages'] = ["This person with the same name and number already exists!"];
            print render_template("tpl/main.html", $data);
        }
    } else header("Location: ?cmd=view");
}

function add_todo_item($item) {
    if (isset($item) && $item != "") {
        $todoItems = get_todo_items();
        if (!in_array($item, $todoItems)) {
            file_put_contents(DATA_FILE, $item . PHP_EOL, FILE_APPEND);
        }
    }
}

function get_todo_items() {
    return file(DATA_FILE, FILE_IGNORE_NEW_LINES);
}

function delete_all_todo_items() {
    file_put_contents(DATA_FILE, "");
}

function delete_todo_item($name) {
    $todoItems = get_todo_items();
    delete_all_todo_items();
    foreach ($todoItems as $item) {
        if ($item != $name) {
            add_todo_item($item);
        }
    }
}