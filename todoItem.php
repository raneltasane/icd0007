<?php

class TodoItem {
    public $person_id;
    public $firstname;
    public $lastname;
    public $phones = [];

    function __construct($firstname, $lastname, $phone, $person_id = null)
    {
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->add_phone($phone);
        $this->person_id = $person_id;
    }

    function add_phone($phone){
        $this->phones[] = $phone;
    }

    function get_all_phones(){
        return implode(" | ", $this->phones);
    }
}